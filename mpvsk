#!/bin/sh
# Launches mpv with a unique mpv socket at /tmp/mpvsockets by default
# You can set the MPV_SOCKET_DIR environment variable to override the location

: "${MPV_SOCKET_DIR:=${TMPDIR:-/tmp}/mpvsockets}" # Default location if MPV_SOCKET_DIR unset or null
[ "$MPV_PATH" ] && MPV="$MPV_PATH/mpv"            # Try getting mpv in MPV_PATH
[ ! "$MPV" ]    && MPV=$(which mpv 2>/dev/null)   # Else try getting mpv in PATH

die(){ cat -; exit $1;} # Exit with message function

if [ ! "$MPV" ]; then
die 2 <<\eof
Could not find the mpv binary in MPV_PATH or PATH
Set the MPV_PATH environment variable to the directory
where the mpv binary resides
eof
fi

# Make sockets directory
mkdir -p "$MPV_SOCKET_DIR" || die $? <<\eof
Could not create mpv sockets directory
eof

quote_args(){
for i do
    q="$i'" r=''
    while [ "$q" ]; do
        r="$r${q%%\'*}'\''" && q=${q#*\'}
    done
    q="'${r%????}'" && q=${q#\'\'}
    eval "q=\${q:-\"''\"}"
    printf "%s\n" "${q} \\"
done
printf " \n"
}
MPV_ARGS=$(quote_args "$@") # Store quoted args in variable

# Need to exec an sh in order to get socket PID
exec /bin/sh <<eof
eval "set -- ${MPV_ARGS}" # Put MPV_ARGS in $@ of this subshell
socket_name="\$$"         # Socket PID (because it will replace this subshell)

# Execute mpv with a socket and quoted args
exec "${MPV}" --input-ipc-server="${MPV_SOCKET_DIR}/\${socket_name}" "\$@"
eof
